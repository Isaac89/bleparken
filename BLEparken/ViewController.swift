//
//  ViewController.swift
//  BLEparken
//
//  Created by Luis Kentzler on 6/9/18.
//  Copyright © 2018 Luis Kentzler. All rights reserved.
//

import UIKit
import CoreBluetooth

// To know if it's on or off
var onOrOff : Bool = true

//Service UUID FFF0
let serviceLED = CBUUID.init(string: "FFF0")

//Characteristic UUID FFF3
let charLED = CBUUID.init(string: "FFF3")











class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    //central manager
    var centralManager : CBCentralManager!
    
    //store peripheral
    var myPeripheral : CBPeripheral?
    
    //Register Button
    @IBOutlet weak var registerButton: UIButton!
    

    
    //CENTRAL MANAGER DELEGATE
    
    //central is our CBCentral
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
       
        //checks if our central is powered on
        if central.state == CBManagerState.poweredOn{
            //Gives command to central to star scanning for Perihperals
            central.scanForPeripherals(withServices: nil, options: nil)
            print("Scanning...")
        }
    }
    
    //callback for when peripheral is discovered (part of the delegate)
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        //print the name of the peripheral (if no name, write "no name")
        //print(peripheral.name ?? "no name")
        
        //if TEAM APRIL is found, print its name
        if peripheral.name?.contains("TEAM APRIL") == true {
            
            
            //tell the centralManager to stop scanning
            centralManager.stopScan()
            
            
            //connect to peripheral
            central.connect(peripheral, options: nil) //go to didConnect callback
           
            //store peripheral locally
            myPeripheral = peripheral
            
            //print advertisement data of TEAM APRIL
            //print(advertisementData)
            
        }
    }
    
    
    
     //callback for when peripheral is connected (part of the delegate)
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
       
        
        //call discoverServices to discover the services of the peripheral (and implement the callback didDiscoverServices)
        peripheral.discoverServices(nil) //pass nil beacause we are interested in all services
        peripheral.delegate = self
        
        
        //call discoverCharacteristics to discover the characteristics of the service (and implement the callback didDiscoverCharacteristics)
        
    }
    
    
    
     //callback for when peripheral is disconnected (part of the delegate)
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        //start to scan again when we disconnect from peripheral
        central.scanForPeripherals(withServices: nil, options: nil)
    }
    
    
    
    // PERIPHERAL DELEGATE
    
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        //binding to a constant to sort through all services
        if let services = peripheral.services {
            for svc in services {
                print(svc.uuid)
                //only filter the service of FFF0
                if svc.uuid == serviceLED {
                    //print the UUID of each service (in String)
                    //print(svc.uuid.uuidString)
                    
                    //discover its characteristics
                    peripheral.discoverCharacteristics(nil, for: svc) //nil because we want all characteristics, svc it's the current service we are iterating through (because of the if, it's ServiceLED FF0. Need callback to didDiscoverCharacteristics
                    
                }
            }
        }
    }
 
 
  
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        //iterate through the service characteristic
        if let chars = service.characteristics {
            for char in chars {
                if char.uuid == charLED{
                    if char.properties.contains(CBCharacteristicProperties.writeWithoutResponse){
                        peripheral.writeValue(Data.init(bytes: [05]), for: char, type: CBCharacteristicWriteType.withoutResponse)
                        
                    }
                    else {
                        peripheral.writeValue(Data.init(bytes: [05]), for: char, type: CBCharacteristicWriteType.withResponse)
                        
                    }
                }
                
            }
            
        }
    }
    
    //callback for didWriteValueFor
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {

    }
    
    
    
    //button action
    @IBAction func activateBluetooth(_ sender: Any) {
        if let service = myPeripheral?.services {
            for svc in service {
                //only filter the service of FFF0
                if svc.uuid == serviceLED {
                    if let chars = svc.characteristics {
                        for char in chars {
                            
                            if char.uuid == charLED{
                                if char.properties.contains(CBCharacteristicProperties.writeWithoutResponse){
                                    myPeripheral?.writeValue(Data.init(bytes: [03]), for: char, type: CBCharacteristicWriteType.withoutResponse)
                                    
                                }
                                else {
                                    if onOrOff{myPeripheral?.writeValue(Data.init(bytes: [03]), for: char, type: CBCharacteristicWriteType.withResponse)
                                        sleep(2)
                                        myPeripheral?.writeValue(Data.init(bytes: [05]), for: char, type: CBCharacteristicWriteType.withResponse)
                                        
                                        onOrOff = false
                                    }
                                    else {
                                        myPeripheral?.writeValue(Data.init(bytes: [03]), for: char, type: CBCharacteristicWriteType.withResponse)
                                        sleep(2)
                                        myPeripheral?.writeValue(Data.init(bytes: [05]), for: char, type: CBCharacteristicWriteType.withResponse)
                                        
                                        onOrOff = true
                                    }
                                    
                                    
                                }
                            }
                            
                        }
                        
                    }
                    
                    
                }
            }
            
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        centralManager = CBCentralManager.init(delegate: self, queue: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

